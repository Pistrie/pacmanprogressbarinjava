## What is this?

My attempt at making a pacman progress bar, based on how the one from the Arch package manager looks.

Currently the progress of our little pacman is dependant on the percentage, so it doesn't really do what it's supposed to do :P

I don't know if I am going to turn this into a fully functional progress bar. Maybe if I ever write a program that requires a progress bar!
